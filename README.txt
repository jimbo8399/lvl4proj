
=====================================================================
Local PCA-based outlier detection and Voting algorithm

*** all versions of the files together with the timeline of the project
could be found on the BitBucket repository under the link:
https://bitbucket.org/jimbo8399/lvl4proj ***
=====================================================================

Txt files:
coord.txt
data.txt

- All data provided by the Berkeley Intel Lab
(http://db.csail.mit.edu/labdata/labdata.html)

=====================================================================

Script:
helpers.py

- A library containing all additional methods needed for the execution of the
outlier detection algorithm and analysis
- No output

=====================================================================

Script:
closest3.py

- Given an input file coord.txt finds the neighbourhoods for a list of 
specified sensors, where the size of each neighbourhood is of size 4
- The result of the script is a TXT file containing a list of neighbourhoods,
formatted to be used in a neighbourhood processing script.
The output file is under the name 

./sensorsAndGroups.txt

=====================================================================

Scripts:
truthTableAnalysisPolicy1LessVisible.py
truthTableAnalysisPolicy1Obvious.py
truthTableAnalysisPolicy2LessVisible.py
truthTableAnalysisPolicy2Obvious.py
truthTableAnalysisPolicy3LessVisible.py
truthTableAnalysisPolicy3Obvious.py

- Performs a truth tables test on each policy where the processed data
has either "Less visible" or "Obvious" outliers
- The output of the script is a command-line table for each angle from 1 to 90 degrees for each specified neighbourhood.

=====================================================================

Scripts:
analysisPolicy1LessVisible.py
analysisPolicy1Obvious.py
analysisPolicy2LessVisible.py
analysisPolicy2Obvious.py
analysisPolicy3LessVisible.py
analysisPolicy3Obvious.py

- Perform analysis on the "helpful", "misleading", "inherent", "consistent"
outlier predictions for a list of specified neighbourhoods
- The results of the script is a SVG barchart image store under the location:

imgs/[#neighbourhood#]/Policy#/lessVisible#outlierProbability#Analysis

======================================================================

Scripts:
analysisPolicy1DynamicHeatmapLessVisible.py
analysisPolicy1DynamicHeatmapObvious.py
analysisPolicy2DynamicHeatmapLessVisible.py
analysisPolicy2DynamicHeatmapObvious.py

- Perform analysis on the differences between a neighbourhood's source 
eigenvector angle and neighbour eigenvector angle. 
- The result of the script is a SVG heatmap image stored under the location:

imgs/heatmapsLessVisibleP1
imgs/heatmapsLessVisibleP2
imgs/heatmapsObviosP1
imgs/heatmapsObviosP2

=====================================================================

Scripts:
outlierVotingP1.py
outlierVotingP2.py
outlierVotingP3.py

- Runs the outlier detection algorithm for each policy on a specified group of
neighbourhoods with a static threshold of 45 degrees.
- Scripts allow modifications for:
	TOLERANCE = power method tolerance
	ALLOWED_MAX_NANS = maximum allowed invalid percentage per sensor (0 to 1)
	SAMPLE_SIZE = training data size
	TEST_SIZE = streaming data size
	THRESHOLD = static threshold (0 to 90)
- The output of the script is a command-line output on each datapoint 
detected as an outlier and its deviation angle.

=====================================================================

Scripts:
outlierVotingP1DynamicThreshold.py
outlierVotingP2DynamicThreshold.py
outlierVotingP3DynamicThreshold.py

- Runs the outlier detection algorithm for each policy on a specified group of
neighbourhoods with a dynamic threshold calculated individually for each 
sensor in the neighbourhood.
- The output of the script is a command-line output on each datapoint 
detected as an outlier and its deviation angle.
- Scripts allow modifications for:
	TOLERANCE = power method tolerance
	ALLOWED_MAX_NANS = maximum allowed invalid percentage per sensor (0 to 1)
	SAMPLE_SIZE = training data size
	TEST_SIZE = streaming data size
- The algorithm also shows an individual summary table on the execution for 
each neighbourhood

=====================================================================

Scripts:
policy1evaluationDynamicLessVisible.py
policy1evaluationDynamicObvious.py
policy1evaluationDynamic.py
policy2evaluationDynamicLessVisible.py
policy2evaluationDynamicObvious.py
policy2evaluationDynamic.py
policy3evaluationDynamicLessVisible.py
policy3evaluationDynamicObvious.py
policy3evaluationDynamic.py

- Perform an evaluation on the three policies where the processed data
has either "Less visible" or "Obvious" outliers
- The output of the script is a SVG barchart image stored under the location:

imgs/evaluationDynamic/[#neighbourhood#]/Policy#

=====================================================================

Scripts:
boxplot_eigenvectors_plotting.py
findEigenvaluesDiff.py
hist_eigenvectors_plotting.py
sensorprocessing.py

- Additional analysis, which show the plotted results either in a web browser
using the plotly platform or in a new window using the matplotlib library
- No recorded output