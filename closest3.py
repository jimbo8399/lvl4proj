# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 11:37:14 2017

@author: 2133352a
"""
import math

def getIndexSmallest(values):
    ind = 0
    for i in range(1,len(values)):
        if values[ind]>values[i]:
            ind = i  
    return ind

def getClosest3(sensor):
    coord = {}
    distances = []
    output = ""
    f = open("coord.txt",'r')
    for line in f.readlines():
        line = line.strip().split()
        coord[int(line[0])] = [float(line[1]), float(line[2])] 
    f.close()
    
    for key in coord.keys():
        if key == sensor or key == 5 or key == 28:
            distances += [float('inf')]
        else:
            x = coord[key][0] - coord[sensor][0]
            y = coord[key][1] - coord[sensor][1]
            distances += [math.sqrt(x**2 + y**2)]
            
    for i in range(3):
        smallestInd = getIndexSmallest(distances)
        output += (","+str(smallestInd+1))
        distances[smallestInd] = float('inf')
    return output

f = open("sensorsAndGroups.txt",'w')

#sensors = [7,20,21,23,45,46,47,48]
sensors = [x for x in range(1,55)]

for i in range(len(sensors)):
	if sensors[i]!=5 and sensors[i]!=28:
	    f.write("["+str(sensors[i]) + str(getClosest3(sensors[i]))+ "],")
    
f.close()
