# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 17:05:16 2018

@author: 2133352a
"""

import pandas as pd
import numpy as np

import datetime as dt
import math

import time
import random

import os

import helpers as h

timestamp = dt.datetime.now().strftime('%d_%m_%H_%M_%S')
TOLERANCE = 0.001
ALPHA = 0.1
ALLOWED_MAX_NANS = 0.2
PROB = 0.33

#ALWAYS KEEP GROUP SIZE AN ODD NUMBER 
#groups = [[7,10,5,8],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]
#TAKING INTO ACCOUNT THAT sensors 5 and 28 have more than 20% of NaN values
groups = [[7,10,8,6],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]

df = pd.read_csv("data.txt", delim_whitespace=True)

df.columns = ['date', 'time', 'epoch', 'moteid',
              'temperature', 'humidity', 'light', 'voltage']


df.insert(0, 'datetime', pd.to_datetime(df.date + ' ' + df.time))
df = df[['datetime', 'moteid', 'temperature', 'humidity', 'light', 'voltage']]
df = df.dropna(axis=0,how='any')
df = df.sort_values(by='datetime')

destroyedRecord = {}
normalRecord = {}

consistent = {}
misleading = {}
inherent = {}
helping = {}
tr = {}
fls = {}

for GROUP in groups:
    print("~~~~~~~~~~",GROUP,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    SAMPLE = 1000
    
    #Train data for first N datapoints for each Sensor from the group
    df1, mean_vecs, std_devs, cov_mats, eig_vecs, thresholds = h.train_model_from_group_sensor_data(SAMPLE,GROUP,df,TOLERANCE)
    #################################################################
    print(GROUP)
    print(thresholds)
    group = GROUP
    for i in range(len(group)):
        SAMPLE = 1000
        UPPERBOUND = SAMPLE+100           
        timestamps= []
        group = group[1:]+[group[0]]
        MOTEID = group[0]
        consistent[MOTEID] = 0
        misleading[MOTEID] = 0
        inherent[MOTEID] = 0
        helping[MOTEID] = 0
        tr[MOTEID] = 0
        fls[MOTEID] = 0
        
        print("\n--------------PROCESSING--SENSOR--%d-----------------------\n" % MOTEID)
        print(time.strftime("%H:%M:%S"))
        
        destroyedRecord[MOTEID] = []
        normalRecord[MOTEID] = []
        
        try:
    
            init_df = df[df.moteid == MOTEID]
            init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
            
            ##########AMENDING DATA WITH PROBABLITY 0.01#####################################
            probList = h.probabilityList(UPPERBOUND-SAMPLE,PROB)
            for q in range(SAMPLE,UPPERBOUND):
                if probList[q-SAMPLE]==1:
                    alpha = random.choice([-1,1])
                    init_df.iloc[q,1] = init_df.iloc[q,1] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    init_df.iloc[q,2] = init_df.iloc[q,2] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    init_df.iloc[q,3] = init_df.iloc[q,3] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    init_df.iloc[q,4] = init_df.iloc[q,4] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    destroyedRecord[MOTEID] += init_df[q:q+1].index.tolist()
                else:
                    normalRecord[MOTEID] += init_df[q:q+1].index.tolist()
            
            #################################################################################
        
            mean_vec = mean_vecs[0]
            std_dev = std_devs[0]
            cov_mat = cov_mats[0]
            eig_vec = eig_vecs[0]
            ###############################################
            
        except ValueError:
            print("\n~Error with MOTEID %d\n" % MOTEID)
        except IndexError:
            print("Index Error")
        except KeyboardInterrupt:
            print("Keyboard Interrupt")
        
        init_df = df[df.moteid == MOTEID]
        init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
        size = init_df.shape[0]
        
        mean_vec = mean_vecs[i]
        std_dev = std_devs[i]
        cov_mat = cov_mats[i]
        eig_vec = eig_vecs[i]
        
        while(SAMPLE<UPPERBOUND and SAMPLE<size):
        
            ############################################################################
            #HANDLING NEW DATA POINT#
            ############################################################################
            
            incoming = init_df[SAMPLE:SAMPLE+1]
            new_datapoint = np.matrix(incoming[['temperature', 'humidity', 'light', 'voltage']])
            
            #The new Mean and Standard Deviation vector with the new datapoint
            new_mean_vec = h.online_mean(SAMPLE+1,mean_vec,new_datapoint)
            new_std_dev = h.online_standard_dev(SAMPLE+1,mean_vec.T,new_mean_vec.T,std_dev.T,new_datapoint.T)
            adj_mat = (new_datapoint - new_mean_vec)/new_std_dev
            new_cov_mat = cov_mat+(1/(SAMPLE+1))*((adj_mat*adj_mat.T)-cov_mat)
    
            #POWER_METHOD#
            (new_eig_val,new_eig_vec) = h.power_method(new_cov_mat,TOLERANCE)
            ############################################################################
            
            #Check if the eigenvectors are with length 1
            #WHILE NOT: iterate and normalize~~~~~~~~~~~~~~~~
            eig_vec, new_eig_vec = h.normalize(eig_vec,new_eig_vec)
    
            eigenvectors_dot_product = (np.dot(eig_vec.T,new_eig_vec)).item(0)
            if(eigenvectors_dot_product>1 and np.isclose(eigenvectors_dot_product,1)):
                eigenvectors_dot_product = 1
                
            elif(eigenvectors_dot_product<-1 and np.isclose(eigenvectors_dot_product,-1)):
                eigenvectors_dot_product = -1
                
            angle = math.degrees(np.arccos(eigenvectors_dot_product))
            if angle > thresholds[0] :
                sensorVote = 1 #Outlier
            else:
                sensorVote = 0 #NOT Outlier
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            groupVote = 0
            for j in range(1,len(group)):
                vote, ang = h.checkOutlier(SAMPLE+1, new_datapoint,mean_vecs[j],std_devs[j],cov_mats[j],eig_vecs[j],TOLERANCE,thresholds[j])
                groupVote += vote
            
            if (groupVote < 2): # --> Normal
                if sensorVote==1:
                    ###################
                    #Analysis LOCAL=1 | GROUP= 0 | REALITY = 1
                    if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                        inherent[MOTEID] += 1
                    #Analysis LOCAL=1 | GROUP= 0 | REALITY = 0
                    if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                        helping[MOTEID] += 1
                    ###################
                else:
                    if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                        fls[MOTEID] += 1
                    if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                        tr[MOTEID] += 1
                mean_vec = new_mean_vec
                cov_mat = new_cov_mat
                std_dev = np.matrix(new_std_dev)
                df1.append(incoming)
                eig_val = new_eig_val
                eig_vec = new_eig_vec
            else: # --> Outlier
                if sensorVote==1:
                    ###################
                    #Analysis LOCAL=1 | GROUP= 1 | REALITY = 1
                    if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                        consistent[MOTEID] += 1
                    #Analysis LOCAL=1 | GROUP= 1 | REALITY = 0
                    if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                        misleading[MOTEID] += 1
                    ###################
                else:
                    if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                        tr[MOTEID] += 1
                    if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                        fls[MOTEID] += 1
                std_dev = np.matrix(std_dev)
                timestamps += incoming['datetime'].tolist()
            SAMPLE+=1
            ##end of while loop###
print("C ",consistent)
print("M ",misleading)
print("I ",inherent)
print("H ",helping)
print("T ",tr)
print("F ",fls)

import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(5, 5))

print(type(list(consistent.keys())))
print(type(consistent.values()))
index = np.ndarray(len(consistent.keys()))
print(consistent.keys())
bar_width = 0.25
opacity = 0.4

if not os.path.exists('imgs/evaluationDynamic/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Probability/'):
        os.makedirs('imgs/evaluationDynamic/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Probability/')

rects1 = plt.bar(index-2*bar_width, tuple(consistent.values()), bar_width,
                alpha=opacity, color='#ffff66',
                label='Consistent')

rects2 = plt.bar(index-bar_width, tuple(misleading.values()), bar_width,
                alpha=opacity, color='#cc0099',
                label='Misleading')

rects3 = plt.bar(index, tuple(inherent.values()), bar_width,
                alpha=opacity, color='#ff6600',
                label='Inherent')

rects4 = plt.bar(index + bar_width, tuple(helping.values()), bar_width,
                alpha=opacity, color='#00cc00',
                label='Helping')

plt.set_xlabel('Sensors')
plt.set_ylabel('Scores')
plt.set_title('Consistent, Inherent, Misleading, Helping datapoints score')
plt.set_xticks(index + bar_width / 2)
#ax.set_xticklabels(('A', 'B', 'C', 'D', 'E'))
plt.legend()

plt.tight_layout()
plt.show()

plt.draw() # necessary to render figure before saving
fig.savefig('imgs/evaluationDynamic/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Probability/Inherent-'+str(group)+ '.svg', bbox_inches='tight')