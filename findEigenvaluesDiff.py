

f = open("EIGreport17_10_10_25_55.txt","r")
lines = f.readlines()
sens = [s for s in range(1,55) if s not in [5,15,17,28]]
diff = []
print(sens)
print(lines[5])
for i in range(len(lines)):
    if "Eigenvalues" in lines[i]:
        li = lines[i+1].strip()
        li = li[1:]
        li = li[:-1]
        li = [float(x) for x in li.split()]
        diff += [li[0]-li[1]]

for i in range(len(sens)):
    print(sens[i],diff[i])

import plotly.plotly as py
import plotly.graph_objs as go
import plotly.tools as pt

pt.set_credentials_file(username='2133352a', api_key='3aw9R3J5v49bKLCBy5Hc')

data = [go.Bar(
            x=sens,
            y=diff
    )]

layout = go.Layout(
    xaxis=dict(title="Sensors"),
    yaxis=dict(title="Difference"),
    title="Difference between 1st and 2nd eigenvalue"
)

fig = go.Figure(data=data, layout=layout)

py.plot(fig, filename="eigenvalue-difference")
        
    
