# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 13:58:52 2018

@author: 2133352a

Policy 1: Opinion-driven policy
:: Here the datapoints are judged to be
:: outliers - based on the group decision
:: not outliers - based on the group data
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as py

import datetime as dt
import math

import time
import random
import os

import helpers as h

##############################################################################
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##############################################################################

timestamp = dt.datetime.now().strftime('%d_%m_%H_%M_%S')
TOLERANCE = 0.001
ALPHA = 0.1
ALLOWED_MAX_NANS = 0.2
PROB = 0.33

#ALWAYS KEEP GROUP SIZE AN ODD NUMBER 
#groups = [[7,10,5,8],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]
#TAKING INTO ACCOUNT THAT sensors 5 and 28 have more than 20% of NaN values
groups = [[7,10,8,6],[20,21,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[48,47,49,52]]

for g in groups:

    #JUST FOR 1 GROUP
    GROUP = g
    group = GROUP
    SAMPLE = 1000
    angles = [i for i in range(1,90)]

    #Process file
    df = pd.read_csv("data.txt", delim_whitespace=True)
        
    df.columns = ['date', 'time', 'epoch', 'moteid',
                  'temperature', 'humidity', 'light', 'voltage']
    
    df.insert(0, 'datetime', pd.to_datetime(df.date + ' ' + df.time))
    df = df[['datetime', 'moteid', 'temperature', 'humidity', 'light', 'voltage']]
    df = df.dropna(axis=0,how='any')
    df = df.sort_values(by='datetime')

    #Train data for first N datapoints for each Sensor from the group
    df1, mean_vecs, std_devs, cov_mats, eig_vecs = h.simple_train_model_from_group_sensor_data(SAMPLE,group,df,TOLERANCE)
    #################################################################
    
    
    for i in range(len(group)):
        #----EVALUATION-----#
        falseRate = []
        trueRate = []
        #####################
    
        #-------------------
        
        destroyedRecord = {}
        normalRecord = {}
        
        consistent = {}
        misleading = {}
        inherent = {}
        helping = {}
        
        group = group[1:]+[group[0]]
        mean_vecs = mean_vecs[1:] + [mean_vecs[0]]
        std_devs = std_devs[1:] + [std_devs[0]]
        cov_mats = cov_mats[1:] + [cov_mats[0]]
        eig_vecs = eig_vecs[1:] + [eig_vecs[0]]
        
        MOTEID = group[0]
        SAMPLE = 1000
        UPPERBOUND = SAMPLE+100
        
        print("~~~~~~~~~~",group,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        
        
        destroyedRecord[MOTEID] = []
        normalRecord[MOTEID] = []
        
        timestamps= []
        outliersDegrees = []
        
        print("\n--------------PROCESSING--SENSOR--%d-----------------------\n" % MOTEID)
        print(time.strftime("%H:%M:%S"))
        
        try:
            init_df = df[df.moteid == MOTEID]
            init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
            
            ##########AMENDING DATA WITH PROBABLITY 0.01#####################################
            probList = h.probabilityList(UPPERBOUND-SAMPLE,PROB)
            for q in range(SAMPLE,UPPERBOUND):
                if probList[q-SAMPLE]==1:
                    alpha = random.choice([-1,1])
                    init_df.iloc[q,1] = init_df.iloc[q,1] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    init_df.iloc[q,2] = init_df.iloc[q,2] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    init_df.iloc[q,3] = init_df.iloc[q,3] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    init_df.iloc[q,4] = init_df.iloc[q,4] + alpha * 0.5 * std_devs[0].tolist()[0][0]
                    destroyedRecord[MOTEID] += init_df[q:q+1].index.tolist()
                else:
                    normalRecord[MOTEID] += init_df[q:q+1].index.tolist()
            
            #################################################################################
        
            # mean_vec = mean_vecs[0]
            # std_dev = std_devs[0]
            # cov_mat = cov_mats[0]
            # eig_vec = eig_vecs[0]
            ###############################################
            
        except ValueError:
            print("\n~Error with MOTEID %d\n" % MOTEID)
        except IndexError:
            print("Index Error")
        except KeyboardInterrupt:
            print("Keyboard Interrupt")
            
        for threshold in angles:
            consistent[threshold] = 0
            misleading[threshold] = 0
            inherent[threshold] = 0
            helping[threshold] = 0
            SAMPLE = 1000
            UPPERBOUND = SAMPLE+100
            mean_vec = mean_vecs[0]
            std_dev = std_devs[0]
            cov_mat = cov_mats[0]
            eig_vec = eig_vecs[0]
            
            while(SAMPLE<UPPERBOUND):
            
                #HANDLING NEW DATA POINT#
                
                ############################################################################
                
                incoming = init_df[SAMPLE:SAMPLE+1]
                new_datapoint = np.matrix(incoming[['temperature', 'humidity', 'light', 'voltage']])
                
                #The new Mean and Standard Deviation vector with the new datapoint
                new_mean_vec = h.online_mean(SAMPLE+1,mean_vec,new_datapoint)
                new_std_dev = h.online_standard_dev(SAMPLE+1,mean_vec.T,new_mean_vec.T,std_dev.T,new_datapoint.T)
                adj_mat = (new_datapoint - new_mean_vec)/new_std_dev
                new_cov_mat = cov_mat+(1/(SAMPLE+1))*((adj_mat*adj_mat.T)-cov_mat)
            
                #POWER_METHOD#
                (new_eig_val,new_eig_vec) = h.power_method(new_cov_mat,TOLERANCE)
                ############################################################################
                
                #Check if the eigenvectors are with length 1
                #WHILE NOT: iterate and normalize~~~~~~~~~~~~~~~~
                eig_vec, new_eig_vec = h.normalize(eig_vec,new_eig_vec)
            
                eigenvectors_dot_product = (np.dot(eig_vec.T,new_eig_vec)).item(0)
                if(eigenvectors_dot_product>1 and np.isclose(eigenvectors_dot_product,1)):
                    eigenvectors_dot_product = 1
                    
                elif(eigenvectors_dot_product<-1 and np.isclose(eigenvectors_dot_product,-1)):
                    eigenvectors_dot_product = -1
                    
                angle = math.degrees(np.arccos(eigenvectors_dot_product))
                if angle > threshold :
                    sensorVote = 1 #Outlier
                else:
                    sensorVote = 0 #NOT Outlier
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                
                groupVote = 0
                for j in range(len(group)):
                    if MOTEID != group[j]:
                        vote, a = h.checkOutlier(SAMPLE+1,new_datapoint,mean_vecs[j],std_devs[j],cov_mats[j],eig_vecs[j],TOLERANCE,threshold)
                        groupVote += vote
                        
                if groupVote >=2:
                    groupVote = 1
                else:
                    groupVote = 0
                
                #Updating or Not that is the question
                if groupVote==1:
                    if sensorVote==1:
                        ###################
                        #Analysis LOCAL=1 | GROUP= 1 | REALITY = 1
                        if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                            consistent[threshold] += 1
                        #Analysis LOCAL=1 | GROUP= 1 | REALITY = 0
                        if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                            inherent[threshold] += 1
                        ###################
                        std_dev = np.matrix(std_dev)
                        outliersDegrees += [angle]
                        timestamps += incoming['datetime'].tolist()
                    else:
                        std_dev = np.matrix(std_dev)
                        outliersDegrees += [angle]
                        timestamps += incoming['datetime'].tolist()
                else:
                    if sensorVote==1:
                        ###################
                        #Analysis LOCAL=1 | GROUP= 0 | REALITY = 1
                        if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                            misleading[threshold] += 1
                        #Analysis LOCAL=1 | GROUP= 0 | REALITY = 0
                        if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                            helping[threshold] += 1
                        ###################
                        mean_vec = new_mean_vec
                        cov_mat = new_cov_mat
                        std_dev = np.matrix(new_std_dev)
                        df1.append(incoming)
                        eig_val = new_eig_val
                        eig_vec = new_eig_vec
                        
                    else:
                        
                        mean_vec = new_mean_vec
                        cov_mat = new_cov_mat
                        std_dev = np.matrix(new_std_dev)
                        df1.append(incoming)
                        eig_val = new_eig_val
                        eig_vec = new_eig_vec
                    
                SAMPLE+=1
                ##end of while loop###
        print("CONSISTENT\n")
        con = [consistent[key] for key in range(1,90)]
        print(len(con),con)
        print("MISLEADING\n")
        mis = [misleading[key] for key in range(1,90)]
        print(len(mis),mis)
        print("INHERENT\n")
        inh = [inherent[key] for key in range(1,90)]
        print(len(inh),inh)
        print("HELPING\n")
        hel = [helping[key] for key in range(1,90)]
        print(len(hel),hel)
        
        ########################################################
        index = np.arange(1,90)
        bar_width = 0.9
        if not os.path.exists('imgs/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Analysis/'):
            os.makedirs('imgs/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Analysis/')
        ###############PLOT CONSISTENT##########################
        fig, ax = py.subplots(figsize=(9, 5))
        
        rects = py.bar(index, con, bar_width,
                         align='center',
                         color='#FF9933',
                         label='Consistent opinion rate')
        
        py.xlabel('Angles')
        py.ylabel('Rate')
        py.title('Consistency opinion rate '+str(GROUP))
         
        py.tight_layout()
        #py.show()
        
        py.draw() # necessary to render figure before saving
        fig.savefig('imgs/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Analysis/Consistent-'+str(group)+ '.svg', bbox_inches='tight')
        py.close(fig)
        ###############PLOT MISLEADING##########################
    
        fig, ax = py.subplots(figsize=(9, 5))
        
        rects = py.bar(index, mis, bar_width,
                         align='center',
                         color='#FF0000',
                         label='Misleading opinion rate')
        
        py.xlabel('Angles')
        py.ylabel('Rate')
        py.title('Misleading opinion rate '+str(group))
         
        py.tight_layout()
        #py.show()
        
        py.draw() # necessary to render figure before saving
        fig.savefig('imgs/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Analysis/Misleading-'+str(group)+ '.svg', bbox_inches='tight')
        py.close(fig)
        ###############PLOT INHERENT##########################
    
        fig, ax = py.subplots(figsize=(9, 5))
        
        rects = py.bar(index, inh, bar_width,
                         align='center',
                         color='#0066FF',
                         label='Inherent opinion rate')
        
        py.xlabel('Angles')
        py.ylabel('Rate')
        py.title('Inherent opinion rate '+str(group))
         
        py.tight_layout()
        #py.show()
        
        py.draw() # necessary to render figure before saving
        fig.savefig('imgs/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Analysis/Inherent-'+str(group)+ '.svg', bbox_inches='tight')
        py.close(fig)
        ###############PLOT HELPING############################
    
        fig, ax = py.subplots(figsize=(9, 5))
        
        rects = py.bar(index, hel, bar_width,
                         align='center',
                         color='#33CC33',
                         label='Helping opinion rate')
        
        py.xlabel('Angles')
        py.ylabel('Rate')
        py.title('Helping opinion rate '+str(group))
         
        py.tight_layout()
        #py.show()
        
        py.draw() # necessary to render figure before saving
        fig.savefig('imgs/'+str(GROUP)+'/Policy1/lessVisible'+str(int(PROB*100))+'Analysis/Helping-'+str(group)+ '.svg', bbox_inches='tight')
        py.close(fig)
        #######################################################
        
