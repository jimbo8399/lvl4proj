# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 16:35:03 2017

@author: 2133352a
"""

import pandas as pd
import numpy as np

import datetime as dt
import math

import time

import helpers as h

timestamp = dt.datetime.now().strftime('%d_%m_%H_%M_%S')

TOLERANCE = 0.001
ALLOWED_MAX_NANS = 0.2
SAMPLE_SIZE = 1000
TEST_SIZE = 300 
THRESHOLD = 45

#ALWAYS KEEP GROUP SIZE AN ODD NUMBER 
#groups = [[7,10,5,8],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]
#TAKING INTO ACCOUNT THAT sensors 5 and 28 have more than 20% of NaN values
groups = [[7,10,8,6],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]

df = pd.read_csv("data.txt", delim_whitespace=True)

df.columns = ['date', 'time', 'epoch', 'moteid',
              'temperature', 'humidity', 'light', 'voltage']

df.insert(0, 'datetime', pd.to_datetime(df.date + ' ' + df.time))
df = df[['datetime', 'moteid', 'temperature', 'humidity', 'light', 'voltage']]
df = df.dropna(axis=0,how='any')
df = df.sort_values(by='datetime')

for group in groups:
    print("~~~~~~~~~~",group,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    std_devs = []
    
    mean_vecs = []
    
    cov_mats = []
    
    eig_vecs = []
    
    for i in range(len(group)):
        MOTEID = group[i]
        SAMPLE = SAMPLE_SIZE
        
        timestamps= []
        outliersDegrees = []
        print("\n--------------INITIALIZING--SENSOR--%d-----------------------\n" % MOTEID)
        print(time.strftime("%H:%M:%S"))
        
        try:
            init_df = df[df.moteid == MOTEID]
            init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
            df1 = init_df[0:SAMPLE][['temperature', 'humidity', 'light', 'voltage']]
            data = np.matrix(df1)
            mean_vec = np.matrix(np.mean(data,axis=0))
            mean_vecs += [mean_vec]
            std_dev = np.std(data,axis=0)
            std_devs += [std_dev]
            data_std = (data-mean_vec)/std_dev # standardizing the data <-> (X-X.mean)/X.std_dev
            cov_mat = np.cov(data_std.T)
            cov_mats += [cov_mat]
            size = init_df.shape[0]
            ###############################################
            (eig_val,eig_vec) = h.power_method(cov_mat,TOLERANCE)
            ###############################################
            eig_vecs += [eig_vec]
        except ValueError:
            print("\n~Error with MOTEID %d\n" % MOTEID)
        except IndexError:
            print("Index Error")

    MOTEID = group[0]
    SAMPLE = SAMPLE_SIZE
    UPPERBOUND = SAMPLE+TEST_SIZE
    
    timestamps= []
    outliersDegrees = []
    
#    file.write("\n--------------PROCESSING--SENSOR--%d-----------------------\n" % MOTEID)
    print("\n--------------PROCESSING--SENSOR--%d-----------------------\n" % MOTEID)
    print(time.strftime("%H:%M:%S"))
    
    try:

        init_df = df[df.moteid == MOTEID]
        init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
        df1 = init_df[0:SAMPLE][['temperature', 'humidity', 'light', 'voltage']]
            
        data = np.matrix(df1)

        mean_vec = np.matrix(np.mean(data,axis=0))
        mean_vecs += [mean_vec]
        std_dev = np.std(data,axis=0)
        std_devs += [std_dev]
        
        # standardizing the data <-> (X-X.mean)/X.std_dev
        data_std = (data-mean_vec)/std_dev
        cov_mat = np.cov(data_std.T)
        cov_mats += [cov_mat]
        
        size = init_df.shape[0]
        ###############################################
        (eig_val,eig_vec) = h.power_method(cov_mat,TOLERANCE)
        ###############################################
        eig_vecs += [eig_vec]
        
        while(SAMPLE<UPPERBOUND):
        
            #HANDLING NEW DATA POINT#
            
            ############################################################################
            
            incoming = init_df[SAMPLE:SAMPLE+1]
            new_datapoint = np.matrix(incoming[['temperature', 'humidity', 'light', 'voltage']])

            #The new Mean and Standard Deviation vector with the new datapoint
            new_mean_vec = h.online_mean(SAMPLE+1,mean_vec,new_datapoint)
            new_std_dev = h.online_standard_dev(SAMPLE+1,mean_vec.T,new_mean_vec.T,std_dev.T,new_datapoint.T)
            adj_mat = (new_datapoint - new_mean_vec)/new_std_dev
            new_cov_mat = cov_mat+(1/(SAMPLE+1))*((adj_mat*adj_mat.T)-cov_mat)

            #POWER_METHOD#
            (new_eig_val,new_eig_vec) = h.power_method(new_cov_mat,TOLERANCE)
            ############################################################################
            
            #Check if the eigenvectors are with length 1
            #WHILE NOT: iterate and normalize~~~~~~~~~~~~~~~~
            eig_vec, new_eig_vec = h.normalize(eig_vec, new_eig_vec)
                        
            eigenvectors_dot_product = (np.dot(eig_vec.T,new_eig_vec)).item(0)
            if(eigenvectors_dot_product>1 and np.isclose(eigenvectors_dot_product,1)):
                eigenvectors_dot_product = 1
                
            elif(eigenvectors_dot_product<-1 and np.isclose(eigenvectors_dot_product,-1)):
                eigenvectors_dot_product = -1
                
            angle = math.degrees(np.arccos(eigenvectors_dot_product))
            if angle > THRESHOLD :
                sensorVote = 1 #Outlier
            else:
                sensorVote = 0 #NOT Outlier
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            #Updating or Not that is the question
            if (sensorVote==1):
                std_dev = np.matrix(std_dev)
                outliersDegrees += [angle]
                timestamps += incoming['datetime'].tolist()
                print("_________")
                print(incoming)
                print(angle)
            else:
                mean_vec = new_mean_vec
                cov_mat = new_cov_mat
                std_dev = np.matrix(new_std_dev)
                df1.append(incoming)
                eig_val = new_eig_val
                eig_vec = new_eig_vec
                
            SAMPLE+=1
            ##end of while loop###
            
    except ValueError:
        print("\n~Error with MOTEID %d\n" % MOTEID)
    except IndexError:
        print("Index Error")
