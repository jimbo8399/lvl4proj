# -*- coding: utf-8 -*-
"""
Created on Thu Jan 18 13:13:08 2018

@author: 2133352a
"""

import random
import math
import numpy as np

def probabilityList(size,prob):
    arr = [0]*size
    n = int(prob*size)
    while n>0:
      ind = random.randint(0,size-1)
      if arr[ind]==0:
        arr[ind] = 1
        n-=1
    return arr


def online_mean(alpha, mean_vec, data_point):
    return (mean_vec + (1/alpha)*(data_point-mean_vec))

# new_std_dev = std_dev+ALPHA*(new_datapoint-std_dev)
# https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
# Online algorithms section
def online_standard_dev(alpha, mean_vec, new_mean_vec, std_dev_vec, data_point):
    new_std_dev = []
    for i in range(len(mean_vec)):
        val = math.sqrt((((alpha-1)*(std_dev_vec[i]**2))+(data_point[i]-mean_vec[i])*(data_point[i]-new_mean_vec[i]))/alpha)
        new_std_dev+=[val]
    return [new_std_dev]

def online_covariance_matrix(alpha, cov_mat, adj_mat):
    return cov_mat+(1/alpha)*((adj_mat*adj_mat.T)-cov_mat)
            
def isOutlier(eigenvector, new_eigenvector, threshold):
    return (np.dot(eigenvector.T,new_eigenvector)).item(0) < math.cos(math.radians(threshold))

def normalize(eig_vec,new_eig_vec):
    len_vec = np.linalg.norm(eig_vec)
    len_new_vec = np.linalg.norm(new_eig_vec)
    while len_vec>1.0 or len_new_vec>1.0 :
        #print("---\n%d\n%1.60f , %1.60f\n---" % (SAMPLE,np.linalg.norm(eig_vec),np.linalg.norm(new_eig_vec)))
        if (len_vec>1.0):
            eig_vec = eig_vec/np.linalg.norm(eig_vec)
            len_vec = np.linalg.norm(eig_vec)
        else:
            new_eig_vec = new_eig_vec/np.linalg.norm(new_eig_vec)
            len_new_vec = np.linalg.norm(new_eig_vec)
    return (eig_vec, new_eig_vec)


def power_method(mat, toler):
    ###http://statweb.stanford.edu/~susan/courses/b494/index/node40.html###
    dim = len(mat)
    start = np.matrix(np.ones((dim, 1)))
    dd = 1
    x = start
    n = 10
    while dd > toler:
        y = mat * x
        dd = abs(np.linalg.norm(x) - n)
        n = np.linalg.norm(x)
        x = y / n
    vector = x / np.linalg.norm(x)
    value = n
    return (value, vector)

def train_model_from_group_sensor_data(SAMPLE,group,df, tolerance):
    std_devs = []
    mean_vecs = []
    cov_mats = []
    eig_vecs = []
    thresholds = []
    
    for i in range(len(group)):
        MOTEID = group[i]
        
        #print("\n--------------INITIALIZING--SENSOR--%d-----------------------\n" % MOTEID)
        #print(time.strftime("%H:%M:%S"))
        
        try:
            init_df = df[df.moteid == MOTEID]                                               # query datapoints only with the corresponding MOTEID
            init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]   # get only needed dimensions
            df1 = init_df[0:SAMPLE][['temperature', 'humidity', 'light', 'voltage']]        # get first SAMPLE points and their four dimensions
            data = np.matrix(df1)                                                           # convert to numpy matrix
            # get the mean vector of the SAMPLE data
            mean_vec = np.matrix(np.mean(data,axis=0))
            mean_vecs += [mean_vec]
            # get standard deviation of the data
            std_dev = np.std(data,axis=0)
            std_devs += [std_dev]
            # standardizing the data <-> (X-X.mean)/X.std_dev
            data_std = (data-mean_vec)/std_dev
            # get the covariance matrix
            cov_mat = np.cov(data_std.T)
            cov_mats += [cov_mat]
            ###############################################
            (eig_val,eig_vec) = power_method(cov_mat,tolerance)
            ###############################################
            eig_vecs += [eig_vec]
            ###############################################
            thresholds += [getDynamicThreshold(SAMPLE,i,init_df, mean_vecs,std_devs,cov_mats,eig_vecs,tolerance)]
        except ValueError:
            print("\n~Error with MOTEID %d\n" % MOTEID)
        except IndexError:
            print("Index Error")
        except KeyboardInterrupt:
            print("Keyboard Interrupt")
    return (df1, mean_vecs, std_devs, cov_mats, eig_vecs, thresholds)

def simple_train_model_from_group_sensor_data(SAMPLE,group,df, tolerance):
    std_devs = []
    mean_vecs = []
    cov_mats = []
    eig_vecs = []
    
    for i in range(len(group)):
        MOTEID = group[i]
        
        #print("\n--------------INITIALIZING--SENSOR--%d-----------------------\n" % MOTEID)
        #print(time.strftime("%H:%M:%S"))
        
        try:
            init_df = df[df.moteid == MOTEID]                                               # query datapoints only with the corresponding MOTEID
            init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]   # get only needed dimensions
            df1 = init_df[0:SAMPLE][['temperature', 'humidity', 'light', 'voltage']]        # get first SAMPLE points and their four dimensions
            data = np.matrix(df1)                                                           # convert to numpy matrix
            # get the mean vector of the SAMPLE data
            mean_vec = np.matrix(np.mean(data,axis=0))
            mean_vecs += [mean_vec]
            # get standard deviation of the data
            std_dev = np.std(data,axis=0)
            std_devs += [std_dev]
            # standardizing the data <-> (X-X.mean)/X.std_dev
            data_std = (data-mean_vec)/std_dev
            # get the covariance matrix
            cov_mat = np.cov(data_std.T)
            cov_mats += [cov_mat]
            ###############################################
            (eig_val,eig_vec) = power_method(cov_mat,tolerance)
            ###############################################
            eig_vecs += [eig_vec]
        except ValueError:
            print("\n~Error with MOTEID %d\n" % MOTEID)
        except IndexError:
            print("Index Error")
        except KeyboardInterrupt:
            print("Keyboard Interrupt")
    return (df1, mean_vecs, std_devs, cov_mats, eig_vecs)
            
def checkOutlier(alpha,new_datapoint,mean_vec,std_dev,cov_mat,eig_vec, tolerance, threshold):
    mv = online_mean(alpha,mean_vec,new_datapoint).T
    
    sd = online_standard_dev(alpha,mean_vec.T,mv,std_dev.T,new_datapoint.T)
    
    amat = (new_datapoint - mv)/sd
    cm = online_covariance_matrix(alpha,cov_mat,amat)
    ## calculate new eigen vector for current sensor
    (e_val,e_vec) = power_method(cm,tolerance)

    while np.linalg.norm(eig_vec)>1.0 or np.linalg.norm(e_vec)>1.0 :
        eig_vec = eig_vec/np.linalg.norm(eig_vec)
        e_vec = e_vec/np.linalg.norm(e_vec)
    eigenvectors_dot_product = np.dot(eig_vec.T,e_vec)
    angle = math.degrees(np.arccos(eigenvectors_dot_product))
    if angle > threshold :
        return 1,angle #Outlier
    else:
        return 0,angle #NOT Outlier
    
def getDynamicThreshold(SAMPLE,moteid_index, init_df, mean_vecs, std_devs, cov_mats, eig_vecs, tolerance):
    THRESHOLDSAMPLE = SAMPLE
    THRESHOLDSAMPLELIMIT = THRESHOLDSAMPLE+100
    thresholdangles = []
    
    while(THRESHOLDSAMPLE < THRESHOLDSAMPLELIMIT):
        #HANDLING NEW DATA POINT#
        
        ############################################################################
        
        incoming = init_df[THRESHOLDSAMPLE:THRESHOLDSAMPLE+1]
        new_datapoint = np.matrix(incoming[['temperature', 'humidity', 'light', 'voltage']])
        
        #The new Mean and Standard Deviation vector with the new datapoint
        new_mean_vec = online_mean(THRESHOLDSAMPLE+1,mean_vecs[0],new_datapoint)
        new_std_dev = online_standard_dev(THRESHOLDSAMPLE+1,mean_vecs[0].T,new_mean_vec.T,std_devs[0].T,new_datapoint.T)
        adj_mat = (new_datapoint - new_mean_vec)/new_std_dev
        new_cov_mat = cov_mats[0]+(1/(THRESHOLDSAMPLE+1))*((adj_mat*adj_mat.T)-cov_mats[0])
    
        #POWER_METHOD#
        (new_eig_val,new_eig_vec) = power_method(new_cov_mat,tolerance)
        ############################################################################
        
        #Check if the eigenvectors are with length 1
        #WHILE NOT: iterate and normalize~~~~~~~~~~~~~~~~
        len_vec = np.linalg.norm(eig_vecs[0])
        len_new_vec = np.linalg.norm(new_eig_vec)
        while len_vec>1.0 or len_new_vec>1.0 :
            if (len_vec>1.0):
                eig_vecs[0] = eig_vecs[0]/np.linalg.norm(eig_vecs[0])
                len_vec = np.linalg.norm(eig_vecs[0])
            else:
                new_eig_vec = new_eig_vec/np.linalg.norm(new_eig_vec)
                len_new_vec = np.linalg.norm(new_eig_vec)
    
        eigenvectors_dot_product = (np.dot(eig_vecs[0].T,new_eig_vec)).item(0)
        if(eigenvectors_dot_product>1 and np.isclose(eigenvectors_dot_product,1)):
            eigenvectors_dot_product = 1
            
        elif(eigenvectors_dot_product<-1 and np.isclose(eigenvectors_dot_product,-1)):
            eigenvectors_dot_product = -1
            
        angle = math.degrees(np.arccos(eigenvectors_dot_product))
        thresholdangles += [angle]
        THRESHOLDSAMPLE += 1
    q3 = np.percentile(thresholdangles,75)
    q1 = np.percentile(thresholdangles,25)
    iqr = q3-q1
    mildThreshold = q3 + 1.5 * iqr
    return mildThreshold

def generateLessVisibleOutliers(df, MOTEID, UPPERBOUND, SAMPLE, PROB, destroyedRecord, normalRecord, std_dev):
    init_df = df[df.moteid == MOTEID]
    init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
    
    ##########AMENDING DATA WITH PROBABLITY 0.01#####################################
    probList = probabilityList(UPPERBOUND-SAMPLE,PROB)
    for q in range(SAMPLE,UPPERBOUND):
        if probList[q-SAMPLE]==1:
            alpha = random.choice([-1,1])
            init_df.iloc[q,1] = init_df.iloc[q,1] + alpha * 0.5 * std_dev.tolist()[0][0]
            init_df.iloc[q,2] = init_df.iloc[q,2] + alpha * 0.5 * std_dev.tolist()[0][0]
            init_df.iloc[q,3] = init_df.iloc[q,3] + alpha * 0.5 * std_dev.tolist()[0][0]
            init_df.iloc[q,4] = init_df.iloc[q,4] + alpha * 0.5 * std_dev.tolist()[0][0]
            destroyedRecord[MOTEID] += init_df[q:q+1].index.tolist()
        else:
            normalRecord[MOTEID] += init_df[q:q+1].index.tolist()
            
    return init_df

def generateObviousOutliers(df, MOTEID, UPPERBOUND, SAMPLE, PROB, destroyedRecord, normalRecord, std_dev,mean_vec):
    init_df = df[df.moteid == MOTEID]
    init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
    
    ##########AMENDING DATA WITH PROBABLITY 0.01#####################################
    probList = probabilityList(UPPERBOUND-SAMPLE,PROB)
    for q in range(SAMPLE,UPPERBOUND):
        if probList[q-SAMPLE]==1:
            init_df.iloc[q,1] = init_df.iloc[q,1] + 4 * std_dev.tolist()[0][0] * mean_vec.tolist()[0][0]
            init_df.iloc[q,2] = init_df.iloc[q,2] + 7 * std_dev.tolist()[0][0]**2 * 3
            init_df.iloc[q,3] = init_df.iloc[q,3] + 3 * mean_vec.tolist()[0][0]**2 * 3
            init_df.iloc[q,4] = init_df.iloc[q,4] + 3 * mean_vec.tolist()[0][0]
            destroyedRecord[MOTEID] += init_df[q:q+1].index.tolist()
        else:
            normalRecord[MOTEID] += init_df[q:q+1].index.tolist()
    
    return init_df
    #################################################################################

