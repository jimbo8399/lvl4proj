# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 16:59:38 2017

@author: Ekaterina Aleksandrova
"""

import pandas as pd
import numpy as np

def plotInScatterplot(df2):
    #plotting the data into scatterplot
    columns = ['datetime','temperature','humidity','light','voltage']
    for c1 in range(len(columns)):
        for c2 in range(c1+1,len(columns)):
            trace = go.Scatter(
                    x = df2[columns[c1]],
                    y = df2[columns[c2]],
                    mode = 'markers'
            )
            if (df2[columns[c1]].isnull().sum()>0):
                print(columns[c1])
            if (df2[columns[c2]].isnull().sum()>0):
                print(columns[c2])
            data = [trace]
            py.iplot(data, filename='%s-%s-scatter'%(columns[c1],columns[c2]))

def eigenvalsvecs(cols):
    # COMPUTE FIRST EIGENVALUE AND EIGENVECTOR
    vals, vecs = np.linalg.eig(cov_mat)

    order = vals.argsort()[::-1]
    file.write('\nSort order\n%s' % order)

    cols = columns[order]
    vals = vals[order]
    vecs = vecs[:, order]

    return cols, vals, vecs


def power_method(mat, toler):
    """
    Does maxit iterations of the power method
    on the matrix mat starting at start.
    Returns an approximation of the largest
    eigenvector of the matrix mat.
    """
    dim = len(mat)
    start = np.matrix(np.ones((dim, 1)))
    dd = 1
    x = start
    n = 10
    while dd > toler:
        y = mat * x
        dd = abs(np.linalg.norm(x) - n)
        n = np.linalg.norm(x)
        x = y / n
    vector = x / np.linalg.norm(x)
    value = n
    return [value], vector


##############################################################################
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##############################################################################
import datetime as dt

import plotly.tools as pt
import plotly.plotly as py
import plotly.graph_objs as go

# pt.set_credentials_file(username='jimbo8399', api_key='Xd6bFWOUsFOo9UOm6sct')
pt.set_credentials_file(username='2133352a', api_key='3aw9R3J5v49bKLCBy5Hc')

timestamp = dt.datetime.now().strftime('%d_%m_%H_%M_%S')
TOLERANCE = 0.001

df = pd.read_csv("data.txt", delim_whitespace=True)

df.columns = ['date', 'time', 'epoch', 'moteid',
              'temperature', 'humidity', 'light', 'voltage']

groupedEigValues = {1: [], 2: [], 3: [], 4: []}
groupedVariance = {1: [], 2: [], 3: [], 4: []}
groupedEuDist = {1: [], 2: [], 3: [], 4: []}
groupedSensors = {1: [], 2: [], 3: [], 4: []}

file = open("EIGreport" + timestamp + ".txt", 'w')

for i in range(1, 55):
    MOTEID = i
    SAMPLE = 1000
    file.write("\n---------------------SENSOR--%d-----------------------\n" % i)
    print("\n---------------------SENSOR--%d-----------------------\n" % i)

    df1 = df[df.moteid == MOTEID]

    df1.insert(0, 'datetime', pd.to_datetime(df1.date + ' ' + df1.time))

    df2 = df1[['datetime', 'epoch', 'moteid', 'temperature', 'humidity', 'light', 'voltage']]
    df2 = df2.sort_values(by='datetime')

    df2 = df2[0:SAMPLE]

    ################################################################################

    #plotInScatterplot(df2)
        
    ################################################################################

    df2 = df2[['temperature', 'humidity', 'light', 'voltage']]
    columns = np.array(['temperature', 'humidity', 'light', 'voltage'])

    try:
        from sklearn.preprocessing import StandardScaler

        # standardizing the data <-> (X-X.mean)/X.std_dev
        X_std = StandardScaler().fit_transform(df2)

        cov_mat = np.cov(X_std.T)
        file.write('\nCovariance matrix \n%s' % cov_mat)

        ###############################################
        (columns, eig_vals, eig_vecs) = eigenvalsvecs(columns)

        file.write('\nSorted Columns \n%s' % columns)
        file.write('\nSorted Eigenvectors \n%s' % eig_vecs)
        file.write('\nSorted Eigenvalues \n%s\n' % eig_vals)

        #eu_all = []

        #varPercentage = []

        # START for pc in range(1,5):
        current_total = 0
        for pc in range(1, len(eig_vals)+1):

            file.write("\n~PC~%d~\n" % pc)
            print("\n~PC~%d~" % pc)
            
            groupedEigValues[pc] += [eig_vals[pc - 1]]
            
            total = sum(eig_vals)
            current_total += eig_vals[pc-1]
            groupedVariance[pc] += [(current_total / total) * 100]
            print("\n%d PC explains %2.5f%% of the data" % (pc, groupedVariance[pc][-1]))
            file.write("\n%d PC explains %2.5f%% of the data" % (pc, groupedVariance[pc][-1]))

            compressed_data = (X_std.dot(eig_vecs[:, 0:pc])).T
            file.write("\nCompressed data\n%s" % compressed_data)

            decompressed_data = (eig_vecs[:, 0:pc].dot(compressed_data)).T
            file.write("\nDecompressed data\n%s" % decompressed_data)

            eu_dist = np.linalg.norm(decompressed_data - X_std)
            file.write("\nEuclidean distance with MOTEID %d: %s\n" % (MOTEID, eu_dist))

            groupedEuDist[pc] += [eu_dist]
            groupedSensors[pc] += [str(MOTEID)]
            
    except ValueError:
        file.write("\n~Error with MOTEID %d\n" % MOTEID)
        print("\n~Error with MOTEID %d\n" % MOTEID)
        # END for pc in range(1,5):
        ############################################################################

varianceSensors = {}
for pc in range(1,len(eig_vals)+1):
    # plotting the % variance explanation in a Brachart for 1PC, 2PCs and 3PCs
    groupedVariance[pc] = np.array(groupedVariance[pc])
    groupedSensors[pc] = np.array(groupedSensors[pc])

    sort_order = groupedVariance[pc].argsort()[::-1]
    varianceSensors[pc] = groupedSensors[pc][sort_order].tolist()
    groupedVariance[pc] = groupedVariance[pc][sort_order]

    totalSum = 0
    for var in groupedVariance[pc]:
        totalSum += var
    print("Average percentage explaining variance for %d is: %f" % (pc, totalSum / len(groupedVariance[pc])))
    
    #plotting compression error rates in a Barchart for 1PC, 2PCs, 3PCs and 4PCs
    #sorted in descending order by compression error rate
    data = [go.Bar(
            x = varianceSensors,
            y = groupedVariance[pc]
            )]
    chart_title = 'Barchart for % Variance explanation for ' + str(pc) + ' PC'
    
    chart_name = 'PC' + str(pc) +'-sortedByVariancePercentage-barchart_' + timestamp
    layout = go.Layout(
        title=chart_title,
        yaxis = dict(
            range=[50,100]        
        ),
        xaxis = dict(
            autorange='True',
            type = "category"
        )
    )
    fig = go.Figure(data=data, layout=layout)
    #py.iplot(fig,filename=chart_name)
    

        ############################################################################
    #sensors = np.array(sensors)
    #eu_all = np.array(eu_all)
    groupedEigValues[pc] = np.array(groupedEigValues[pc])
    
    sort_order = groupedEigValues[pc].argsort()[::-1]
    eigenvaluesSensors = groupedSensors[pc][sort_order].tolist()
    groupedEigValues[pc] = groupedEigValues[pc][sort_order]

    #plotting eigenvalues in a Barchart for 1PC, 2PCs, 3PCs and 4PCs
    #sorted in descending order by value
    data = [go.Bar(
            x = eigenvaluesSensors,
            y = groupedEigValues[pc]
            )]    
    chart_title = 'Eigenvalues for PC ' + str(pc)
    chart_name = 'Eigenvalues-PC' + str(pc) +'-sortedByValue-barchart'+timestamp
    layout = go.Layout(
        title=chart_title,
        xaxis = dict(
            autorange='True',
            type = "category"
        )
    )
    fig = go.Figure(data=data, layout=layout)
    #py.iplot(fig,filename=chart_name)
    
    ############################################################################
    groupedEuDist[pc] = np.array(groupedEuDist[pc])
    sort_order = groupedEuDist[pc].argsort()[::-1]
    euSensors = groupedSensors[pc][sort_order].tolist()
    groupedEuDist[pc] = groupedEuDist[pc][sort_order]


    #plotting compression error rates in a Barchart for 1PC, 2PCs, 3PCs and 4PCs
    #sorted in descending order by compression error rate
    data = [go.Bar(
            x = euSensors,
            y = groupedEuDist[pc]
            )]
    chart_title = 'Barchart for PC ' + str(pc)
    chart_name = 'PC' + str(pc) +'-sortedByErrorRate-barchart'
    layout = go.Layout(
        title=chart_title,
        xaxis = dict(
            autorange='True',
            type = "category"
        )
    )
    fig = go.Figure(data=data, layout=layout)
    #py.iplot(fig,filename=chart_name)
    
    ############################################################################

file.close()
