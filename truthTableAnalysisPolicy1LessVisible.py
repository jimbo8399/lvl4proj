# -*- coding: utf-8 -*-
"""
Created on Thu Nov 30 08:09:50 2017

@author: 2133352a

Policy 1: Opinion-driven policy
:: Here the datapoints are judged to be outliers or not
:: based only on the group decision

"""
COUNT = 0

import pandas as pd
import numpy as np

import datetime as dt
import math
import random

import time

import itertools as it

import helpers as h

##############################################################################
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##############################################################################

#----EVALUATION-----#
falseRate = []
trueRate = []
#####################

timestamp = dt.datetime.now().strftime('%d_%m_%H_%M_%S')
TOLERANCE = 0.001
ALLOWED_MAX_NANS = 0.2

#ALWAYS KEEP GROUP SIZE AN ODD NUMBER 
#groups = [[7,10,5,8],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]
#TAKING INTO ACCOUNT THAT sensors 5 and 28 have more than 20% of NaN values
groups = [[7,10,8,6],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]

angles = [i for i in range(1,90)]

df = pd.read_csv("data.txt", delim_whitespace=True)

df.columns = ['date', 'time', 'epoch', 'moteid',
              'temperature', 'humidity', 'light', 'voltage']

df.insert(0, 'datetime', pd.to_datetime(df.date + ' ' + df.time))
df = df[['datetime', 'moteid', 'temperature', 'humidity', 'light', 'voltage']]
df = df.dropna(axis=0,how='any')
df = df.sort_values(by='datetime')

for group in groups:
    destroyedRecord = {}
    normalRecord = {}
    TPoutliersRecord = {}
    TNoutliersRecord = {}
    FPoutliersRecord = {}
    FNoutliersRecord = {}
    print("~~~~~~~~~~",group,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    MOTEID = group[0]
    SAMPLE = 1000
    UPPERBOUND = SAMPLE+100
    
    #Train data for first N datapoints for each Sensor from the group
    df1, mean_vecs, std_devs, cov_mats, eig_vecs = h.simple_train_model_from_group_sensor_data(SAMPLE,group,df,TOLERANCE)
    #################################################################
    
    destroyedRecord[MOTEID] = []
    normalRecord[MOTEID] = []
    TPoutliersRecord[MOTEID] = []
    TNoutliersRecord[MOTEID] = []
    FNoutliersRecord[MOTEID] = []
    FPoutliersRecord[MOTEID] = []
    
    timestamps= []
    outliersDegrees = []
    
    print("\n--------------PROCESSING--SENSOR--%d-----------------------\n" % MOTEID)
    print(time.strftime("%H:%M:%S"))
    
    try:
        ##########AMENDING DATA WITH PROBABLITY 0.05#####################################
        init_df = h.generateLessVisibleOutliers(df, MOTEID, UPPERBOUND, SAMPLE, 0.05, destroyedRecord, normalRecord, std_devs[0])
        #################################################################################
    except ValueError:
        print("\n~Error with MOTEID %d\n" % MOTEID)
    except IndexError:
        print("\nIndex Error")
    except KeyboardInterrupt:
        print("\nKeyboard Interrupt")

    for threshold in angles:
        TPoutliersRecord[MOTEID] = []
        TNoutliersRecord[MOTEID] = []
        FNoutliersRecord[MOTEID] = []
        FPoutliersRecord[MOTEID] = []
        ###############################################
        mean_vec = mean_vecs[0]
        std_dev = std_devs[0]
        cov_mat = cov_mats[0]
        size = init_df.shape[0]
        eig_vec = eig_vecs[0]
        ###############################################
        
        SAMPLE = 1000
        UPPERBOUND = SAMPLE+100 
        while(SAMPLE<UPPERBOUND):
        
            ############################################################################
            #HANDLING NEW DATA POINT#
            ############################################################################
            
            incoming = init_df[SAMPLE:SAMPLE+1]
            new_datapoint = np.matrix(incoming[['temperature', 'humidity', 'light', 'voltage']])
            
            #The new Mean and Standard Deviation vector with the new datapoint
            new_mean_vec = h.online_mean(SAMPLE+1,mean_vec,new_datapoint)
            new_std_dev = h.online_standard_dev(SAMPLE+1,mean_vec.T,new_mean_vec.T,std_dev.T,new_datapoint.T)
            adj_mat = (new_datapoint - new_mean_vec)/new_std_dev
            new_cov_mat = h.online_covariance_matrix(SAMPLE+1, cov_mat, adj_mat)

            #POWER_METHOD#
            (new_eig_val,new_eig_vec) = h.power_method(new_cov_mat,TOLERANCE)
            ############################################################################
            
            #Check if the eigenvectors are with length 1
            #WHILE NOT: iterate and normalize~~~~~~~~~~~~~~~~
            eig_vec, new_eig_vec = h.normalize(eig_vec, new_eig_vec)

            eigenvectors_dot_product = (np.dot(eig_vec.T,new_eig_vec)).item(0)
            if(eigenvectors_dot_product>1 and np.isclose(eigenvectors_dot_product,1)):
                eigenvectors_dot_product = 1
                
            elif(eigenvectors_dot_product<-1 and np.isclose(eigenvectors_dot_product,-1)):
                eigenvectors_dot_product = -1
                
            angle = math.degrees(np.arccos(eigenvectors_dot_product))
            if angle > threshold :
                sensorVote = 1 #Outlier
            else:
                sensorVote = 0 #NOT Outlier
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            groupVote = 0
            for j in range(len(group)):
                if MOTEID != group[j]:
                    vote, ang = h.checkOutlier(SAMPLE+1,new_datapoint,mean_vecs[j],std_devs[j],cov_mats[j],eig_vecs[j],TOLERANCE,threshold)
                    groupVote += vote
            
                
            if (groupVote < 2): # -> NORMAL found
                ###############CHECK NON-OUTLIERS MATCHING WITH TRUE-NON-OUTLIERS###########
                if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                    TNoutliersRecord[MOTEID] += incoming.index.tolist()
                if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                    FNoutliersRecord[MOTEID] += incoming.index.tolist()
                ############################################################################
                mean_vec = new_mean_vec
                cov_mat = new_cov_mat
                std_dev = np.matrix(new_std_dev)
                df1.append(incoming)
                eig_val = new_eig_val
                eig_vec = new_eig_vec
            else: #groupVote >= 2 --> OUTLIER found
                ###############CHECK OUTLIERS MATCHING WITH CREATED OUTLIERS################
                if (incoming.index.tolist()[0] in destroyedRecord[MOTEID]):
                    TPoutliersRecord[MOTEID] += incoming.index.tolist()
                if (incoming.index.tolist()[0] in normalRecord[MOTEID]):
                    FPoutliersRecord[MOTEID] += incoming.index.tolist()
                ############################################################################
                std_dev = np.matrix(std_dev)
                outliersDegrees += [angle]
                timestamps += incoming['datetime'].tolist()
            SAMPLE+=1
            ##end of while loop###
        
        TP = len(list(it.chain(*list(TPoutliersRecord.values()))))
        FP = len(list(it.chain(*list(FPoutliersRecord.values()))))
        TN = len(list(it.chain(*list(TNoutliersRecord.values()))))
        FN = len(list(it.chain(*list(FNoutliersRecord.values()))))
        
        falseRate += [FP+FN]
        trueRate += [TP+TN]

        print("THRESHOLD",threshold)
        print("                POLICY 1\n")
        print("|------------------------------------------|")
        print("| |            PREDICTED                   |")
        print("|A|----------------------------------------|")
        print("|C|    |     Outlier       |    Normal     |")
        print("|T|----|-----------------------------------|")
        print("|U|Outl|      %d            |       %d       |"%(TP,FN))
        print("|A|----|-----------------------------------|")
        print("|L|Norm|      %d           |       %d       |"%(FP,TN))
        print("|------------------------------------------|")

  
# import plotly.plotly as py
# import plotly.graph_objs as go
# import plotly.tools as pt

# #pt.set_credentials_file(username='2133352a', api_key='3aw9R3J5v49bKLCBy5Hc')
# pt.set_credentials_file(username='jimbo8399', api_key='Xd6bFWOUsFOo9UOm6sct')

# trace1 = go.Bar(
#     x = angles,
#     y = trueRate,
#     name = 'True rate'
# )

# trace2 = go.Bar(
#     x = angles,
#     y = falseRate,
#     name = 'False rate'
# )

# layout = go.Layout(
#     title='Policy 1: FalseRate against TrueRate for '+str(groups[0]),
#     barmode='group',
#     xaxis=dict(
#         title='Angles',
#         ),
#     yaxis=dict(
#         title='Rate',
#         )
# )

# data = [trace1,trace2]

# fig = go.Figure(data=data, layout=layout)

# py.iplot(fig, filename='P1:FRate-TRate'+str(groups[0]))
