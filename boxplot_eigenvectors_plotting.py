# -*- coding: utf-8 -*-
"""
Created on Thu Dec  7 17:25:31 2017

@author: 2133352a
"""

COUNT = 0

import pandas as pd
import numpy as np

import datetime as dt
import math

import time

THRESHOLD = 45

def online_mean(alpha, mean_vec, data_point):
    return (mean_vec + (1/alpha)*(data_point-mean_vec))

#new_std_dev = std_dev+ALPHA*(new_datapoint-std_dev)
# https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
# Online algorithms section
def online_standard_dev(alpha, mean_vec, new_mean_vec, std_dev_vec, data_point):
    new_std_dev = []
    for i in range(len(mean_vec)):
        val = math.sqrt((((alpha-1)*(std_dev_vec[i]**2))+(data_point[i]-mean_vec[i])*(data_point[i]-new_mean_vec[i]))/alpha)
        new_std_dev+=[val]
    return [new_std_dev]


def power_method(mat, toler):
    ###http://statweb.stanford.edu/~susan/courses/b494/index/node40.html###
    dim = len(mat)
    start = np.matrix(np.ones((dim, 1)))
    dd = 1
    x = start
    n = 10
    while dd > toler:
        y = mat * x
        dd = abs(np.linalg.norm(x) - n)
        n = np.linalg.norm(x)
        x = y / n
    vector = x / np.linalg.norm(x)
    value = n
    return value, vector


##############################################################################
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
##############################################################################

timestamp = dt.datetime.now().strftime('%d_%m_%H_%M_%S')
TOLERANCE = 0.001
ALPHA = 0.1
ALLOWED_MAX_NANS = 0.2

#ALWAYS KEEP GROUP SIZE AN ODD NUMBER 
#groups = [[7,10,5,8],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]
#TAKING INTO ACCOUNT THAT sensors 5 and 28 have more than 20% of NaN values
#groups = [[7,10,8,6],[20,21,19,22],[21,20,19,22],[23,27,22,21],[45,44,46,43],[46,45,47,48],[47,45,46,48],[48,47,49,52]]

#JUST FOR GROUP OF SIZE 1
#groups = x = [[y] for y in range(1,55) if y not in [5,15,17,28]]

#northeastSensors = [11,12,13,14,16,18,19,21]
#northwestSensors = [4,6,7,8,9,10,46,47,48,49,50,51,52,53,54]
#southeastSensors = [1,2,3,22,23,24,25,26,27,29,30,31,32,33,34,35]
#southwestSensors = [36,37,38,39,40,41,42,43,44,45]
groups = [[19],[20],[21],[22]]

plotlydata = []



df = pd.read_csv("data.txt", delim_whitespace=True)

df.columns = ['date', 'time', 'epoch', 'moteid',
              'temperature', 'humidity', 'light', 'voltage']

df.insert(0, 'datetime', pd.to_datetime(df.date + ' ' + df.time))
df = df[['datetime', 'moteid', 'temperature', 'humidity', 'light', 'voltage']]
df = df.dropna(axis=0,how='any')
df = df.sort_values(by='datetime')


for group in groups:
    print("~~~~~~~~~~",group,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    destroyedRecord = {}
    nonOutliersRecord = {}
    TPoutliersRecord = {}
    TNoutliersRecord = {}
    FPoutliersRecord = {}
    FNoutliersRecord = {}
    std_devs = []
    
    mean_vecs = []
    
    cov_mats = []
    
    eig_vecs = []
    
    MOTEID = group[0]
    SAMPLE = 1000
    
    timestamps= []
    outliersDegrees = []
    print("\n--------------INITIALIZING--SENSOR--%d-----------------------\n" % MOTEID)
    print(time.strftime("%H:%M:%S"))
    
    try:
        init_df = df[df.moteid == MOTEID]
        init_df = init_df[['datetime','temperature', 'humidity', 'light', 'voltage']]
        df1 = init_df[0:SAMPLE][['temperature', 'humidity', 'light', 'voltage']]
        if MOTEID==21:
            print(df1)
        
        data = np.matrix(df1)
        
        mean_vec = np.matrix(np.mean(data,axis=0))
        mean_vecs += [mean_vec]
        std_dev = np.std(data,axis=0)
        std_devs += [std_dev]
        data_std = (data-mean_vec)/std_dev # standardizing the data <-> (X-X.mean)/X.std_dev
        cov_mat = np.cov(data_std.T)
        cov_mats += [cov_mat]
        ###############################################
        (eig_val,eig_vec) = power_method(cov_mat,TOLERANCE)
        ###############################################
        eig_vecs += [eig_vec]
    except ValueError:
        print("\n~Error with MOTEID %d\n" % MOTEID)
    except IndexError:
        print("Index Error")
    except KeyboardInterrupt:
        print("Keyboard Interrupt")
    
    '''
    Threshold model using the next 1000 datapoints
    '''
    THRESHOLDSAMPLE = 1000
    THRESHOLDSAMPLELIMIT = 2000
    thresholdangles = []
    
    while(THRESHOLDSAMPLE<THRESHOLDSAMPLELIMIT):
        #HANDLING NEW DATA POINT#
        
        ############################################################################
        
        incoming = init_df[THRESHOLDSAMPLE:THRESHOLDSAMPLE+1]
        new_datapoint = np.matrix(incoming[['temperature', 'humidity', 'light', 'voltage']])
        
        #The new Mean and Standard Deviation vector with the new datapoint
        new_mean_vec = online_mean(SAMPLE+1,mean_vec,new_datapoint)
        new_std_dev = online_standard_dev(SAMPLE+1,mean_vec.T,new_mean_vec.T,std_dev.T,new_datapoint.T)
        adj_mat = (new_datapoint - new_mean_vec)/new_std_dev
        new_cov_mat = cov_mat+(1/(SAMPLE+1))*((adj_mat*adj_mat.T)-cov_mat)
        
        #POWER_METHOD#
        (new_eig_val,new_eig_vec) = power_method(new_cov_mat,TOLERANCE)
        ############################################################################
        
        #Check if the eigenvectors are with length 1
        #WHILE NOT: iterate and normalize~~~~~~~~~~~~~~~~
        len_vec = np.linalg.norm(eig_vec)
        len_new_vec = np.linalg.norm(new_eig_vec)
        
        #print("---\n%d\n%1.60f , %1.60f\n---" % (SAMPLE,np.linalg.norm(eig_vec),np.linalg.norm(new_eig_vec)))
        while (len_vec>1.0):
            eig_vec = eig_vec/np.linalg.norm(eig_vec)
            len_vec = np.linalg.norm(eig_vec)
        while(len_new_vec>1.0):
            new_eig_vec = new_eig_vec/np.linalg.norm(new_eig_vec)
            len_new_vec = np.linalg.norm(new_eig_vec)

        eigenvectors_dot_product = (np.dot(eig_vec.T,new_eig_vec)).item(0)
        if(eigenvectors_dot_product>1 and np.isclose(eigenvectors_dot_product,1)):
            eigenvectors_dot_product = 1    
        elif(eigenvectors_dot_product<-1 and np.isclose(eigenvectors_dot_product,-1)):
            eigenvectors_dot_product = -1
            
        angle = math.degrees(np.arccos(eigenvectors_dot_product))
        thresholdangles += [angle]
        THRESHOLDSAMPLE += 1
        
    import plotly.plotly as py
    import plotly.graph_objs as go
    import plotly.tools as pt
    
    pt.set_credentials_file(username='jimbo8399', api_key='Xd6bFWOUsFOo9UOm6sct')
    y = np.array(thresholdangles)
    trace = go.Box(
        y=y,
        name = "Sensor "+str(MOTEID)
    )
    plotlydata += [trace]
        
        
        
####################################################################################################################################   
layout = go.Layout(
    title='Boxplot of the angles for sensors ' + str(groups) + 'between the eigenvectors from 1000th to 2000th datapoint',
    yaxis=dict(
        title='Angles (degrees)'
    ),
    bargap=0.2,
    bargroupgap=0.1
)
fig = go.Figure(data=plotlydata, layout=layout)

py.iplot(fig, filename='boxplot_eigenangles_sensors ' + str(groups))

    
''''''